#include <stdlib.h>
#include <time.h>
#include <iostream>

#include "sdl_utils.h"
#include "game.h"

int main(int argc, char* argv[]){
  srand( (unsigned)time( NULL ) );
  if (!initializeGame(argc, argv))
      return -1;
  runGame();
  return 0;
}
