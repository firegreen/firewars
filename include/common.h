#ifndef COMMON_H
#define COMMON_H

#define None nullptr

#include <stdint.h>
#include <math.h>
#include <cstdlib>

const unsigned int DEFAULT_WINDOW_WIDTH = 1080;
const unsigned int DEFAULT_WINDOW_HEIGHT = 720;
const unsigned int UNITS_COUNT = 3;
const unsigned short CELLS_COUNT = 20;
const int MAX_WEIGHT=2000;

enum TextureID {
    LAND_TEX = 0,
    FOREST_TEX,
    DENSE_FOREST_TEX,
    SEA_TEX,
    SEA_BORDER_TEX,
    RED_SOLDIER_TEX,
    BLACK_SOLDIER_TEX,
    BLUE_SOLDIER_TEX,
    TEXT1_TEX,
    TEXT2_TEX,
    TEXT3_TEX,
    TEXTURES_COUNT
};

const char *textureName(TextureID type);

#endif // COMMON_H
