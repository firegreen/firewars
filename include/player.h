#ifndef PLAYER_H
#define PLAYER_H

#include <list>
#include "utils.h"

class Unit;

struct Player
{
    Player();
    Color color;
    std::list<Unit*> units;

    bool isComputer;
};

#endif // PLAYER_H
