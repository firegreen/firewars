#ifndef MAP_H
#define MAP_H


#include "common.h"

struct Unit;

class Map
{
public:
    Map();
};

enum CellType {
    LAND = 0,
    FOREST,
    SEA,
    TYPES_COUNT
};

struct Cell
{
    CellType type;
    TextureID texture;
    Unit* occupant;
};

#endif // MAP_H
