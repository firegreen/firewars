#ifndef SDL_UTILS_H
#define SDL_UTILS_H
#include <string>
#include <SDL2/SDL.h>
#include <GL/gl.h>

#include "utils.h"

bool initializeWindow(std::string windowTitle,
                      int width=640, int height=480, bool fullscreen=false);
void renderWindow();
void renderWindow(Color background);
void loadTexture(const char* filePath, GLuint textureID);
bool getEvent(SDL_Event* event, bool wait=false);

void drawQuad(float x, float y, float width, float height, GLuint textureID=0);
void drawQuad(float x, float y, float width, float height, const Color &color, GLuint textureID=0);

void exitWindow();
#endif // SDL_UTILS_H
