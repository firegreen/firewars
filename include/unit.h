#ifndef UNIT_H
#define UNIT_H

#include <vector>
#include <queue>

#include "common.h"
#include "map.h"


struct Game;
struct AStarNode;

enum MoveType
{
    FOOT,
    WHEEL,
    TRACK,
    MOVE_TYPES_COUNT
};

enum Affiliation
{
    INFANTRY,
    ARMOURED,
    AIR,
    NAVY,
    AFFILIATIONS_COUNT
};

enum UnitType
{
    MACHINE_GUN,
    BAZOOKA,
    TANK,
    UNIT_TYPES_COUNT
};

enum UnitsGroup {
    RED_GROUP,
    BLUE_GROUP,
    BLACK_GROUP,
    UNITS_GROUPS_COUNT
};

enum UnitState {
    DONE,
    MOVED,
    AVAILABLE
};

struct Unit
{
    Unit(const char* name, int pvMax, int power, int attackRange, int mobility, int cost,
         std::vector<TextureID> textures, MoveType moveType, Affiliation affiliation,
         std::vector<float> efficacity);
    Unit(const Unit& model, unsigned short playerID);

    char* name;
    int pvMax;
    int pv;
    int power;
    int attackRange;
    int mobility;
    int cost;
    TextureID textures[UNITS_GROUPS_COUNT];
    MoveType moveType;
    Affiliation affiliation;
    float efficacity[AFFILIATIONS_COUNT];

    int currentDestinationX;
    int currentDestinationY;

    std::queue<std::pair<int, int> > currentPath;

    int speedX;
    int speedY;

    unsigned short playerID;
    int x, y;
    int cellX, cellY;
    UnitState state;

    Unit* target;
    AStarNode* currentAStar;
};

struct AStarNode
{
    AStarNode(AStarNode* parent, int x, int y, const Game *game,
              const Unit *unit, const AStarNode* end);
    AStarNode(AStarNode* parent, int x, int y);

    int x, y;
    float weight;
    float start_weight;
    AStarNode* parent;
    AStarNode* child;
};

Unit* createUnit(UnitType type, unsigned short playerID);

/**
 * @brief fieldWeight
 * @param unit
 * @param cellType
 * @return if the unit can travel this cell, it returns
 * a integer representing how much is difficult for the unit to travel the cell,
 * 0 otherwise
 */
float fieldWeight(const Unit* unit, CellType cellType);

void randomPositionUnit(Unit* unit, Game *game, int min, int max);
void autoMoveUnit(Unit* unit, Game* game);
void autoAttackUnit(Unit* unit, Game* game);
void moveUnit(Unit* unit, Game* game, int cellX, int cellY, bool updatePosition=false);
void unitsFight(Unit* attacker, Unit* defender,  Game* game);
void updateUnit(Unit* unit, Game* game, uint32_t delta);

AStarNode* AStar(const Unit* unit, const Game *game, int targetX, int targetY);

inline int distance(const Unit* A, const Unit* B)
{
    return std::abs(B->cellX - A->cellX) + std::abs(B->cellY - A->cellY);
}
inline int distance(const AStarNode* A, const AStarNode* B)
{
    return std::abs(B->x - A->x) + std::abs(B->y - A->y);
}


inline TextureID unitTexture(const Unit* unit)
{
    return unit->textures[unit->playerID];
}

#endif // UNIT_H
