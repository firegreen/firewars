#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <list>
#include <queue>

void debug(const char* message, ...);
void error(const char* message, ...);
void warning(const char* message, ...);

class Color
{
public:
    Color()
        : r(0u), g(0u), b(0u), a(255) {};

    Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a=255)
        : r(r), g(g), b(b), a(a) {};

    Color(const Color& other)
        : r(other.r), g(other.g), b(other.b), a(other.a) {};

    unsigned char r, g, b;
    unsigned char a;
};

enum Direction
{
    LEFT,
    TOP,
    RIGHT,
    BOTTOM
};

template <typename T>
void clear(std::list<T> list)
{
    while (!list.empty()) {
        list.pop_back();
    }
}

template <typename T>
void clear(std::queue<T> queue)
{
    while (!queue.empty()) {
        queue.pop();
    }
}

#endif // UTILS_H
