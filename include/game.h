#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include "GL/gl.h"

#include "common.h"
#include "map.h"
#include "player.h"



struct Game
{
    bool running;
    uint32_t runningTime;
    int turn;
    Cell** map;
    GLuint textures[TextureID::TEXTURES_COUNT];

    Player players[4];
    unsigned int playersCount;

    Unit* selectedUnit;

    int windowWidth;
    int windowHeight;

    int cellsCount;
    int cellSize;
};

Game* mainGame();
void handleEvent(SDL_Event* event);
void handleWindowSize(int window_width, int window_height);
bool initializeGame(int argc, char *argv[]);
void runGame();
void updateGame(uint32_t delta);
void drawGame();

#endif // GAME_H
