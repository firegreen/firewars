#include "game.h"

#include <iostream>
#include <SDL2/SDL.h>
#include <math.h>
#include <GL/glu.h>

#include "sdl_utils.h"
#include "unit.h"


Game game;

void nextTurn();


Game *mainGame()
{
    return &game;
}

int count_siblings(int i, int j, CellType type)
{
    int count;
    count = game.map[std::max(0, i-1)][j].type == type;
    count += game.map[i][std::max(0, j-1)].type == type;
    count += game.map[std::min(game.cellsCount-1, i+1)][j].type == type;
    count += game.map[i][std::min(game.cellsCount-1, j+1)].type == type;
    return count;
}

void createMap()
{
    game.map = new Cell*[game.cellsCount];

    for (int i=0; i<game.cellsCount; i++)
    {
        game.map[i] = new Cell[game.cellsCount];
        for (int j=0; j<game.cellsCount; j++)
        {
            game.map[i][j].type = (CellType)(rand() % CellType::TYPES_COUNT);
            game.map[i][j].occupant = None;
        }
    }

    /** smooth map **/
    for (int i=0; i<game.cellsCount; i++)
    {
        for (int j=0; j<game.cellsCount; j++)
        {
            if (count_siblings(i,j, FOREST) >= 3)
                game.map[i][j].type = FOREST;
            else if (count_siblings(i,j, SEA) >= 3)
                game.map[i][j].type = SEA;
            else if (game.map[i][j].type == SEA && count_siblings(i,j, SEA) == 0)
                game.map[i][j].type = LAND;
        }
    }

    /** Assign textures **/
    for (int i=0; i<game.cellsCount; i++)
    {
        for (int j=0; j<game.cellsCount; j++)
        {
            switch (game.map[i][j].type) {
            case FOREST:
                if (count_siblings(i,j, FOREST) >= 3)
                    game.map[i][j].texture = DENSE_FOREST_TEX;
                else
                    game.map[i][j].texture = FOREST_TEX;
                break;
            case SEA:
                game.map[i][j].texture = SEA_TEX;
                break;
            case LAND:
                game.map[i][j].texture = LAND_TEX;
                break;
            case TYPES_COUNT:
                break;
            }
        }
    }
}

void createPlayers()
{
    Unit* unit;
    game.players[0].color = Color(255u,45u,15u);
    game.players[1].color = Color(0u,5u,15u);
    for(u_int i=0; i<UNITS_COUNT; ++i)
    {
        unit = createUnit(MACHINE_GUN, 0);
        randomPositionUnit(unit, &game, game.cellsCount/5, game.cellsCount/2);
        game.players[0].units.push_back(unit);
        unit->state = AVAILABLE;

        unit = createUnit(MACHINE_GUN, 1);
        randomPositionUnit(unit, &game, game.cellsCount/2, game.cellsCount/3);
        game.players[1].units.push_back(unit);
        unit->state = AVAILABLE;
    }
    game.players[1].isComputer = true;
    game.playersCount = 2;
}

bool initializeGame(int, char*[])
{
    char texturesPath[256] = "data/textures/%s.png";
    char filePath[256];

    if (!initializeWindow("ImacWars2",DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT))
        return false;

    glEnable(GL_TEXTURE_2D);
    glEnable( GL_BLEND );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glGenTextures(TextureID::TEXTURES_COUNT, game.textures);
    for (int i=TextureID::LAND_TEX; i<TextureID::TEXTURES_COUNT; i++)
    {
        sprintf(filePath, texturesPath, textureName((TextureID) i));
        loadTexture(filePath, game.textures[i]);
    }

    game.running=true;
    game.turn = 0;
    game.runningTime = 0;
    game.selectedUnit = None;
    game.cellsCount = CELLS_COUNT;
    handleWindowSize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);

    createMap();
    createPlayers();

    return true;
}


void handleWindowSize(int windowWidth, int windowHeight)
{
    glViewport(0, 0, windowWidth, windowHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, windowWidth, windowHeight, 0);
    game.windowWidth=windowWidth;
    game.windowHeight=windowHeight;
    game.cellSize = game.windowHeight/game.cellsCount;
}


void handleEvent(SDL_Event* event)
{
    int cellX, cellY;
    int dX, dY;
    int D;
    Player& currentPlayer = game.players[game.turn % game.playersCount];

    switch (event->type) {
    case SDL_QUIT:
        game.running = false;
        break;
    case SDL_WINDOWEVENT_RESIZED:
        handleWindowSize(event->window.data1, event->window.data2);
        break;
    case SDL_KEYUP:
        if (currentPlayer.isComputer)
            break;
        if (event->key.keysym.sym == SDLK_n)
            nextTurn();
        break;
    case SDL_MOUSEBUTTONUP:
        if (currentPlayer.isComputer)
            break;
        cellX = (int)(event->button.x / game.cellSize);
        cellY = (int)(event->button.y / game.cellSize);
        if (cellX < game.cellsCount && cellY < game.cellsCount)
        {
            if (game.selectedUnit && game.selectedUnit->playerID == game.turn % game.playersCount)
            {
                if (game.selectedUnit->state == AVAILABLE)
                {
                    if (!game.map[cellX][cellY].occupant)
                    {
                        dX = cellX - game.selectedUnit->cellX;
                        dY = cellY - game.selectedUnit->cellY;
                        D = std::abs(dX) + std::abs(dY);
                        if (D != 0 && D <= game.selectedUnit->mobility)
                        {
                            moveUnit(game.selectedUnit, &game, cellX, cellY);
                        }
                        else
                        {
                            game.selectedUnit = None;
                        }
                    }
                }
                else if (game.selectedUnit->state == MOVED)
                {
                    if (game.map[cellX][cellY].occupant)
                    {
                        dX = cellX - game.selectedUnit->cellX;
                        dY = cellY - game.selectedUnit->cellY;
                        D = std::abs(dX) + std::abs(dY);
                        if (D != 0 && D <= game.selectedUnit->attackRange)
                        {
                            unitsFight(game.selectedUnit, game.map[cellX][cellY].occupant, &game);
                        }
                    }
                    game.selectedUnit->state = DONE;
                    game.selectedUnit = None;
                }

            }
            else if (game.map[cellX][cellY].occupant && game.map[cellX][cellY].occupant->state != DONE)
                game.selectedUnit = game.map[cellX][cellY].occupant;
        }
        break;
    }
}

void runGame()
{
    SDL_Event event;
    bool event_available;
    int32_t frame_start=SDL_GetTicks();
    int32_t delta=0;
    uint32_t frame = 0;
    while(game.running)
    {
        std::cout << "frame: " << frame << std::endl;
        frame_start = SDL_GetTicks();

        updateGame(delta);
        drawGame();
        renderWindow(Color());

        event_available = getEvent(&event, false);
        while (event_available)
        {
            handleEvent(&event);
            event_available = getEvent(&event, false);
        }
        delta = (SDL_GetTicks()-frame_start);
        if (delta<60)
        {
            SDL_Delay(60-delta);
            delta = 60;
        }
        frame++;
    }
    exitWindow();
}

void nextTurn()
{
    Player* currentPlayer = &game.players[game.turn % game.playersCount];
    for (Unit* unit : currentPlayer->units)
    {
        moveUnit(unit, &game, unit->cellX, unit->cellY, true);
        unit->state = AVAILABLE;
    }
    game.turn++;
    currentPlayer = &game.players[game.turn % game.playersCount];
    for (Unit* unit : currentPlayer->units)
    {
        unit->state = AVAILABLE;
    }
}

void updateGame(uint32_t delta)
{
    game.runningTime += delta;
    bool changeTurn = true;
    for (const Unit* unit : game.players[game.turn % game.playersCount].units)
    {
        if (unit->state != DONE)
        {
            changeTurn = false;
            break;
        }
    }
    if (changeTurn)
        nextTurn();

    Player& currentPlayer = game.players[game.turn % game.playersCount];
    if (currentPlayer.isComputer)
    {
        for (Unit* unit : currentPlayer.units)
        {
            if (unit->state == AVAILABLE){
                autoMoveUnit(unit, &game);
                break;
            }
            if (unit->state == MOVED && unit->currentDestinationX == 0 && unit->currentDestinationY == 0){
                autoAttackUnit(unit, &game);
                break;
            }
            updateUnit(unit, &game, delta);
        }
    }
    else
    {
        for (Unit* unit : currentPlayer.units)
        {
            updateUnit(unit, &game, delta);
        }
    }
}

void drawSeaBorder(int x, int y, int width, int height, Direction direction)
{
    glPushMatrix();
    glTranslatef(x, y, 0);
    switch(direction)
    {
    case LEFT:
        break;
    case TOP:
        glRotatef(90,0,0,1);
        glTranslatef(0, -height, 0);
        break;
    case RIGHT:
        glTranslatef(width, 0, 0);
        glRotatef(180,0,0,1);
        glTranslatef(0, -height, 0);
        break;
    case BOTTOM:
        glTranslatef(0, height, 0);
        glRotatef(270,0,0,1);
        break;
    }
    drawQuad(0,0,width*0.1, height*1.1, game.textures[SEA_BORDER_TEX]);
    glPopMatrix();
}

void drawPlayerBannerHUD(const Player& p, int width, int height){
    glColor3ub(p.color.r, p.color.g, p.color.b);
    glBegin(GL_POLYGON);
        glVertex3f(0, 0, 0);

        glVertex3f(width*3, 0, 0);
        glVertex3f(width*3.1, height*0.1, 0);
        glVertex3f(width*3.2, height*0.3, 0);
        glVertex3f(width*3.25, height*0.5, 0);
        glVertex3f(width*3.2, height*0.7, 0);
        glVertex3f(width*3.1, height*0.9, 0);
        glVertex3f(width*3, height, 0);

        glVertex3f(0, height, 0);
    glEnd();

    // TODO: Use SDL_TFF to add player name
}

void drawGame()
{
    int x = 0;
    int y = 0;
    int range = 0;
    Color* stateColor = None;
    Color normalColor(255,255,255);
    Color doneColor(125,125,125);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor3ub(255u,255u,255u);
    for (int i=0; i<game.cellsCount; i++)
    {
        for (int j=0; j<game.cellsCount; j++)
        {
            x = i*game.cellSize;
            y = j*game.cellSize;
            drawQuad(x, y, game.cellSize, game.cellSize, game.textures[game.map[i][j].texture]);
            if (game.map[i][j].type == SEA)
            {
                if (i>0 && game.map[i-1][j].type != SEA)
                {
                   drawSeaBorder(x, y, game.cellSize, game.cellSize, LEFT);
                }
                if (j>0 && game.map[i][j-1].type != SEA)
                {
                   drawSeaBorder(x, y, game.cellSize, game.cellSize, TOP);
                }
                if (i<game.cellsCount-1 && game.map[i+1][j].type != SEA)
                {
                   drawSeaBorder(x, y, game.cellSize, game.cellSize, RIGHT);
                }
                if (j<game.cellsCount-1 && game.map[i][j+1].type != SEA)
                {
                   drawSeaBorder(x, y, game.cellSize, game.cellSize, BOTTOM);
                }
            }
        }
    }


    for (unsigned int i=0; i<game.playersCount; ++i)
    {
        for (Unit* unit : game.players[i].units)
        {
            if (unit->state == DONE)
                stateColor = &doneColor;
            else
                stateColor = &normalColor;
            drawQuad(unit->x, unit->y, game.cellSize, game.cellSize,
                     *stateColor, game.textures[unitTexture(unit)]);
            if (unit->currentAStar)
            {
                AStarNode* current = unit->currentAStar;
                while(current)
                {
                    drawQuad(current->x*game.cellSize,
                             current->y*game.cellSize,
                             game.cellSize, game.cellSize, Color(255u,255u,100u,125u));
                    current = current->child;
                }
            }
        }
    }

    if (game.selectedUnit)
    {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        drawQuad(game.selectedUnit->x, game.selectedUnit->y, game.cellSize, game.cellSize,
                 game.textures[unitTexture(game.selectedUnit)]);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        if (game.selectedUnit->playerID == game.turn % game.playersCount)
        {
            if (game.selectedUnit->state == MOVED)
                range = game.selectedUnit->attackRange;
            else
                range = game.selectedUnit->mobility;
            for (int moveX=-range; moveX<=range; ++moveX)
            {
                for (int moveY=-range; moveY<=range; ++moveY)
                {
                    if (!moveX && !moveY)
                        continue;
                    if (std::abs(moveX)+std::abs(moveY)<=range)
                    {
                        int cellX = game.selectedUnit->cellX-moveX;
                        int cellY = game.selectedUnit->cellY-moveY;
                        if (cellX < 0 || cellY < 0 || cellX >= game.cellsCount || cellY >= game.cellsCount)
                            continue;
                        if (game.map[cellX][cellY].occupant)
                            continue;
                        if (game.selectedUnit->state == AVAILABLE &&
                                fieldWeight(game.selectedUnit, game.map[cellX][cellY].type)>=MAX_WEIGHT)
                            continue; // TODO: use path finder to replace this condition
                        drawQuad(cellX*game.cellSize,
                                 cellY*game.cellSize,
                                 game.cellSize, game.cellSize, Color(255u,255u,255u,125u));
                    }
                }
            }
        }
    }


    drawPlayerBannerHUD(game.players[game.turn % game.playersCount], game.cellSize, game.cellSize);
}
