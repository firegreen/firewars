#include "sdl_utils.h"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/gl.h>
#include <stdio.h>

SDL_Window* window = NULL;
SDL_GLContext glcontext;

bool initializeWindow(std::string windowTitle, int width, int height, bool fullscreen)
{
    uint32_t flags = SDL_WINDOW_OPENGL;
    if (fullscreen)
        flags |= SDL_WINDOW_FULLSCREEN;
    window = SDL_CreateWindow(windowTitle.data(),
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              width, height, flags);
    glcontext = SDL_GL_CreateContext(window);
    if(glcontext == 0)
    {
        std::cout << SDL_GetError() << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
        return false;
    }
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    return true;
}

void renderWindow()
{
    SDL_GL_SwapWindow(window);
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT);
}


void renderWindow(Color background)
{
    SDL_GL_SwapWindow(window);
    glClearColor(0, 0, 0, 1);
    glClear(GL_DEPTH_BUFFER_BIT);
    glClearColor(background.r, background.g, background.b,1);
    glClear(GL_COLOR_BUFFER_BIT);
}


bool getEvent(SDL_Event* event, bool wait)
{
    int result;
    if (wait)
        result = SDL_WaitEvent(event);
    else
        result = SDL_PollEvent(event);
    return result;
}

void exitWindow()
{
    SDL_GL_DeleteContext(glcontext);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void loadTexture(const char *filePath, GLuint textureID)
{
    int mode;
    SDL_Surface* image= IMG_Load(filePath);
    if(!image) {
        fprintf(stderr, "IMG_Load: %s\n", IMG_GetError());
        return;
    }

    if(image->format->BytesPerPixel == 4) mode = GL_RGBA;
    else mode = GL_RGB;

    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, mode, image->w, image->h, 0, GL_RGBA,
                  GL_UNSIGNED_BYTE, image->pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void drawQuad(float x, float y, float width, float height, GLuint textureID)
{
    glBindTexture(GL_TEXTURE_2D, textureID);
    glColor3ub(255u,255u,255u);
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex3f(x, y, 0);
        glTexCoord2f(1,0); glVertex3f(x + width, y, 0);
        glTexCoord2f(1,1); glVertex3f(x + width, y + height, 0);
        glTexCoord2f(0,1); glVertex3f(x, y + height, 0);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void drawQuad(float x, float y, float width, float height, const Color& color, GLuint textureID)
{
    glBindTexture(GL_TEXTURE_2D, textureID);
    glColor4ub(color.r,color.g,color.b,color.a);
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex3f(x, y, 0);
        glTexCoord2f(1,0); glVertex3f(x + width, y, 0);
        glTexCoord2f(1,1); glVertex3f(x + width, y + height, 0);
        glTexCoord2f(0,1); glVertex3f(x, y + height, 0);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
}
