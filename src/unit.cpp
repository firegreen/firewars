#include <stdexcept>
#include "unit.h"
#include "game.h"


Unit availableUnits[UNIT_TYPES_COUNT] = {
    Unit(
        "Machine Gun", 100, 100, 1, 3, 1000,
        {RED_SOLDIER_TEX, BLUE_SOLDIER_TEX, BLACK_SOLDIER_TEX}, FOOT, INFANTRY,
        {1, 0.2, 0, 0.1}
    ),
    Unit(
        "Bazooka", 110, 110, 1, 2, 1500,
        {RED_SOLDIER_TEX, BLUE_SOLDIER_TEX, BLACK_SOLDIER_TEX}, FOOT, INFANTRY,
        {1, 1, 0, 0.1}
    ),
    Unit(
        "Tank", 150, 100, 1, 6, 7000,
        {RED_SOLDIER_TEX, BLUE_SOLDIER_TEX, BLACK_SOLDIER_TEX}, TRACK, ARMOURED,
        {1.2, 1, 0, 0.1}
    )
};

Unit::Unit(const char *name, int pvMax, int power, int attackRange, int mobility,
           int cost, std::vector<TextureID> textures, MoveType moveType,
           Affiliation affiliation, std::vector<float> efficacity)
    : name(new char[256]), pvMax(pvMax), pv(pvMax), power(power),
      attackRange(attackRange), mobility(mobility),
      cost(cost), moveType(moveType),
      affiliation(affiliation),
      currentDestinationX(0), currentDestinationY(0),
      currentPath(), playerID(0),
      x(0), y(0), cellX(-1), cellY(-1), state(AVAILABLE),
      target(None), currentAStar(None)
{
    strcpy(this->name, name);
    for(int i=0; i<UNITS_GROUPS_COUNT; ++i)
    {
        this->textures[i] = textures[i];
    }
    for(int i=0; i<AFFILIATIONS_COUNT; ++i)
    {
        this->efficacity[i] = efficacity[i];
    }
}


Unit::Unit(const Unit &model, unsigned short playerID)
    : name(new char[256]), pvMax(model.pvMax), pv(model.pvMax), power(model.power),
      attackRange(model.attackRange), mobility(model.mobility),
      cost(model.cost), moveType(model.moveType),
      affiliation(model.affiliation),
      currentDestinationX(0), currentDestinationY(0),
      currentPath(), speedX(0), speedY(0),
      playerID(playerID), x(0), y(0), cellX(-1), cellY(-1),
      state(AVAILABLE), target(None), currentAStar(None)
{
    strcpy(this->name, model.name);
    for(int i=0; i<UNITS_GROUPS_COUNT; ++i)
    {
        this->textures[i] = model.textures[i];
    }
    for(int i=0; i<AFFILIATIONS_COUNT; ++i)
    {
        this->efficacity[i] = model.efficacity[i];
    }
}

Unit* createUnit(UnitType type, unsigned short playerID)
{
    return new Unit(availableUnits[type], playerID);
}


float fieldWeight(const Unit *unit, CellType cellType)
{
    switch(unit->moveType){
    case FOOT:
        switch (cellType) {
        case LAND:
            return 1;
        case FOREST:
            return 1.5;
        case SEA:
            break;
        default:
            break;
        }
    case WHEEL:
        switch (cellType) {
        case LAND:
            return 1.5;
        case FOREST:
            return 3;
        case SEA:
            break;
        default:
            break;
        }
    case TRACK:
        switch (cellType) {
        case LAND:
            return 1;
        case FOREST:
            return 2;
        case SEA:
            break;
        default:
            break;
        }
    default:
        break;
    }
    return MAX_WEIGHT;
}

void randomPositionUnit(Unit *unit, Game* game, int min, int max)
{
    int threshold = max - min;
    int x = (rand() % threshold) + min;
    int y = (rand() % threshold) + min;

    while(fieldWeight(unit, game->map[x][y].type) >= MAX_WEIGHT ||
          game->map[x][y].occupant)
    {
        x = (rand() % threshold) + min;
        y = (rand() % threshold) + min;
    }
    moveUnit(unit, game, x, y, true);
}

void moveUnit(Unit *unit, Game* game, int cellX, int cellY, bool updatePosition)
{
    if (unit->cellX >= 0 && unit->cellY >= 0)
        game->map[unit->cellX][unit->cellY].occupant = None;
    if (game->map[cellX][cellY].occupant)
        throw std::runtime_error ("Cannot move unit in this cell, it's already occupied");
    if (fieldWeight(unit, game->map[cellX][cellY].type) >= MAX_WEIGHT)
        throw std::runtime_error ("Cannot move unit in this cell");

    if (updatePosition)
    {
        unit->x = cellX * game->cellSize;
        unit->y = cellY * game->cellSize;
    }
    else
    {
        int x = unit->cellX, y = unit->cellY;
        int incX = cellX > x ? 1 : -1;
        int incY = cellY > y ? 1 : -1;
        clear<std::pair<int, int> >(unit->currentPath);
        while(x != cellX)
        {
            x += incX;
            unit->currentPath.push(std::pair<int, int>(x, y));
        }
        while(y != cellY)
        {
            y += incY;
            unit->currentPath.push(std::pair<int, int>(x, y));
        }
        unit->currentDestinationX = 0;
        unit->currentDestinationY = 0;
    }

    unit->cellX = cellX;
    unit->cellY = cellY;
    game->map[cellX][cellY].occupant = unit;
    unit->state = MOVED;
}

void updateUnit(Unit *unit, Game *game, uint32_t delta)
{
    if (unit->currentDestinationX <= 0 || unit->currentDestinationY <=0)
    {
        if (!unit->currentPath.empty())
        {
            std::pair<int, int>& destination = unit->currentPath.front();
            unit->currentDestinationX = destination.first;
            unit->currentDestinationY = destination.second;
            unit->speedX = (destination.first*game->cellSize - unit->x) / 10;
            unit->speedY = (destination.second*game->cellSize - unit->y) / 10;
            unit->currentPath.pop();
        }
    }
    if (unit->currentDestinationX > 0)
    {
        int dest = unit->currentDestinationX*game->cellSize;
        unit->x = unit->x + unit->speedX * delta / 30.0f;
        if ((unit->speedX > 0 && unit->x > dest) ||
                (unit->speedX < 0 && unit->x > dest)){
            unit->x = dest;
            unit->speedX = 0;
            unit->currentDestinationX = 0;
        }
    }
    if (unit->currentDestinationY > 0)
    {
        int dest = unit->currentDestinationY*game->cellSize;
        unit->y = unit->y + unit->speedY * delta / 30.0f;
        if ((unit->speedY > 0 && unit->y > dest) ||
                (unit->speedY < 0 && unit->y > dest)){
            unit->y = dest;
            unit->speedY = 0;
            unit->currentDestinationY = 0;
        }
    }
}

void unitsFight(Unit *attacker, Unit *defender, Game *game)
{
    defender->pv -= attacker->power * attacker->pv/(float)attacker->pvMax;
    if (defender->pv <= 0)
    {
        game->map[defender->cellX][defender->cellY].occupant = None;
        game->players[defender->playerID].units.remove(defender);
    }
    attacker->pv -= defender->power * defender->pv/(float)defender->pvMax;
    if (attacker->pv <= 0)
    {
        game->map[attacker->cellX][attacker->cellY].occupant = None;
        game->players[attacker->playerID].units.remove(attacker);
    }
    attacker->state = DONE;
}

void autoAttackUnit(Unit *unit, Game *game)
{
    if (unit->target && unit->target->pv > 0)
    {
        if (distance(unit, unit->target) <= unit->attackRange)
            unitsFight(unit, unit->target, game);
    }
    unit->state = DONE;
}

void autoMoveUnit(Unit *unit, Game *game)
{
    if (unit->target)
    {
        if (unit->target->pv <= 0)
            unit->target = None;
    }

    const Player* target = None;
    int targetID = 0;
    if (!unit->target)
    {
        targetID = rand() % game->playersCount;
        if (targetID == unit->playerID)
            targetID = (targetID+1) % game->playersCount;
        target = &game->players[targetID];

        if (!target->units.size())
            return;
        targetID = rand() % target->units.size();
        int i=0;
        for (Unit* targetUnit : target->units)
        {
            if (i==targetID)
            {
                unit->target = targetUnit;
                break;
            }
            i++;
        }
    }
    unit->currentAStar = AStar(unit, game, unit->target->cellX, unit->target->cellY);
    AStarNode* current = unit->currentAStar;
    float weight = 0;
    while (current->child && weight<=unit->mobility)
    {
        if (game->map[current->child->x][current->child->y].occupant == unit->target)
        {
            break;
        }
        current = current->child;
        weight += fieldWeight(unit, game->map[current->x][current->y].type);
    }
    while(current && game->map[current->x][current->y].occupant)
        current = current->parent;
    if(current)
        moveUnit(unit, game, current->x, current->y);
    else
        unit->state = DONE;
}


bool nodeIsInList(std::list<AStarNode*>& list, int x, int y)
{
    // TODO: Use a BinarySearchTree
    for(AStarNode* node : list)
    {
        if (node->x == x && node->y == y)
            return true;
    }
    return false;
}


AStarNode *AStar(const Unit *unit, const Game *game, int targetX, int targetY)
{
    AStarNode* start = new AStarNode(None, unit->cellX, unit->cellY);
    AStarNode* end = new AStarNode(None, targetX, targetY);
    AStarNode* current = start;
    std::list<AStarNode*> to_visit;
    std::list<AStarNode*> visited;

    while(current->x != targetX || current->y != targetY)
    {
        visited.push_back(current);

        if (current->x>0)
        {
            if (!nodeIsInList(to_visit, current->x-1, current->y) &&
                    !nodeIsInList(visited, current->x-1, current->y))
                to_visit.push_back(new AStarNode(current, current->x-1, current->y,
                                            game, unit, end));
        }
        if (current->y>0)
        {
            if (!nodeIsInList(to_visit, current->x, current->y-1) &&
                    !nodeIsInList(visited, current->x, current->y-1))
                to_visit.push_back(new AStarNode(current, current->x, current->y-1,
                                             game, unit, end));
        }
        if (current->x<game->cellsCount-1)
        {
            if (!nodeIsInList(to_visit, current->x+1, current->y) &&
                    !nodeIsInList(visited, current->x+1, current->y))
                to_visit.push_back(new AStarNode(current, current->x+1, current->y,
                                             game, unit, end));
        }
        if (current->y>game->cellsCount-1)
        {
            if (!nodeIsInList(to_visit, current->x, current->y+1) &&
                    !nodeIsInList(visited, current->x, current->y+1))
                to_visit.push_back(new AStarNode(current, current->x, current->y+1,
                                             game, unit, end));
        }
        if (to_visit.empty())
        {
            current = None;
            break;
        }

        current = to_visit.front();
        for (AStarNode* node : to_visit)
        {
            if (node->weight < current->weight)
            {
                current = node;
            }
        }
        if (current->weight > MAX_WEIGHT)
        {
            current = None;
            break;
        }
        to_visit.remove(current);
    }
    if(current)
    {
        while(current->parent!=None)
        {
            current->parent->child = current;
            current = current->parent;
        }
    }
    return start;
}

AStarNode::AStarNode(AStarNode *parent, int x, int y, const Game* game, const Unit* unit,
                     const AStarNode *end)
    : x(x), y(y), parent(parent), child(None)
{
    if (parent)
        start_weight = parent->start_weight;
    else
        start_weight = 0;
    start_weight += fieldWeight(unit, game->map[x][y].type);
    weight = start_weight + distance(this, end);
}

AStarNode::AStarNode(AStarNode *parent, int x, int y)
    : x(x), y(y), weight(0), start_weight(0), parent(parent), child(None)
    {}
