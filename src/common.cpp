#include "common.h"

const char* textureName(TextureID type)
{
    switch (type) {
    case RED_SOLDIER_TEX:
        return "red_soldier";
    case BLACK_SOLDIER_TEX:
        return "black_soldier";
    case BLUE_SOLDIER_TEX:
        return "blue_soldier";
    case LAND_TEX:
        return "land";
    case FOREST_TEX:
        return "forest";
    case DENSE_FOREST_TEX:
        return "dense_forest";
    case SEA_TEX:
        return "sea";
    case SEA_BORDER_TEX:
        return "sea_border";
    case TEXTURES_COUNT:
        return "none";
    }
    return "none";
}
