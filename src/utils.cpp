#include <stdarg.h>

#include "utils.h"

void error(const char *message, ...){
    va_list args;
    va_start(args, message);
    fprintf(stderr, "ERROR:");
    vfprintf(stderr, message, args);
    fprintf(stderr, "\n");
    va_end(args);
    exit(EXIT_FAILURE);
}

void warning(const char *message, ...)
{
    va_list args;
    va_start(args, message);
    fprintf(stdout, "WARNING:");
    vfprintf(stdout, message, args);
    fprintf(stdout, "\n");
    va_end(args);
}

void debug(const char *message, ...)
{
#ifdef DEBUG
    va_list args;
    va_start(args, message);
    fprintf(stdout, "DEBUG:");
    vfprintf(stdout, message, args);
    fprintf(stdout, "\n\n");
    va_end(args);
#endif
}
